package com.example.routes

import com.example.model.*
import com.google.gson.Gson
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

fun Route.userRoute(){
    route("user"){
        post("/register") {
            val user = call.receive<User>()
            uploadUser()
            if (userTable.contains(user.username)) {
                call.respondText("L'usuari ja existeix", status = HttpStatusCode.Conflict)
                return@post
            }
            else {
                userTable[user.username] = getMd5Digest("${user.username}:$myRealm:${user.password}")
                userList= loadUsersFromFile("./src/main/resources/userJson")
                userList.add(user)
                updateFile("./src/main/resources/userJson", userList)
                call.respondText("Usuari registrat correctament", status = HttpStatusCode.Accepted)
            }
        }
        post("/login") {
            val user = call.receive<User>()
            userTable=uploadUser()
            val userHidden = getMd5Digest("${user.username}:$myRealm:${user.password}")
            println(userTable[user.username])
            println(userHidden)
            if (userTable.containsKey(user.username) && userTable[user.username]?.contentEquals(userHidden) == true) {
                call.respondText("Login correcte", status = HttpStatusCode.Accepted)
                return@post
            }
            else {
                call.respondText("Login incorrecte", status = HttpStatusCode.Conflict)
            }

        }

        get("/{username}/favourites"){
            if (call.parameters["username"].isNullOrBlank()){
                return@get call.respondText("El nom de l'usuari no es correcte.", status = HttpStatusCode.BadRequest)
            }
            userList= loadUsersFromFile("./src/main/resources/userJson")
            for (i in userList){
                if (i.username == call.parameters["username"]){
                    return@get call.respond(i.listOfFavourites)
                }
            }
            call.respondText("No s'ha trovat l'usuari amb nom ${call.parameters["username"]}.", status = HttpStatusCode.NotFound)
        }
        post("/{username}/favourites/{id}") {
            if (call.parameters["username"].isNullOrBlank()){
                return@post call.respondText("El nom de l'usuari no es correcte.", status = HttpStatusCode.BadRequest)
            }
            else if (call.parameters["id"].isNullOrBlank()){
                return@post call.respondText("L'id del cocktail no es correcte.", status = HttpStatusCode.BadRequest)
            }
            userList= loadUsersFromFile("./src/main/resources/userJson")
            for (i in userList){
                println(userList)
                if (i.username == call.parameters["username"]){
                    barra= loadCocktailsFromFile("./src/main/resources/cocktailsJson")
                    println(barra)
                    for (j in barra){
                        if(j.id == call.parameters["id"]){
                            println("hola")
                            i.listOfFavourites.add(j)
                        }
                    }
                }
            }
            updateFile("./src/main/resources/userJson", userList)
            return@post call.respondText("Cocktail afegit correctament a favorits a l'usuari ${call.parameters["username"]}.", status = HttpStatusCode.Accepted)
        }
        put("/{username}/favourites/{id}") {
            println("hola")
            if (call.parameters["username"].isNullOrBlank()){
                return@put call.respondText("El nom de l'usuari no es correcte.", status = HttpStatusCode.BadRequest)
            }
            else if (call.parameters["id"].isNullOrBlank()){
                return@put call.respondText("L'id del cocktail no es correcte.", status = HttpStatusCode.BadRequest)
            }
            var cocktailReceived:Cocktail?=null
            val gson= Gson()
            val id= call.parameters["id"]
            var file= false
            var fileName=""
            val data = call.receiveMultipart()
            println("data")
            data.forEachPart { part ->
                when (part) {
                    is PartData.FormItem -> {
                        if(part.name== "cocktail_data") {
                            cocktailReceived = gson.fromJson(part.value, Cocktail::class.java)
                        }
                    }
                    is PartData.FileItem -> {
                        file= true
                        fileName = part.originalFileName as String
                        var fileBytes = part.streamProvider().readBytes()
                        File("./src/main/resources/uploads/$fileName").writeBytes(fileBytes)
                    }
                    else -> {}
                }}
            println("adios")
            userList= loadUsersFromFile("./src/main/resources/userJson")
            var cocktail = cocktailReceived?.let { it1 -> Cocktail(id.toString(), cocktailReceived!!.name,cocktailReceived!!.alcoholic,cocktailReceived!!.ingredients,cocktailReceived!!.instructions, fileName) }
            for (i in userList){
                if (i.username == call.parameters["username"]){
                    var position=-1
                    for (j in i.listOfFavourites){
                        position++
                        if(j.id == call.parameters["id"]){
                            if (cocktail != null) {
                                if (file==true){
                                    i.listOfFavourites.set(position, cocktail)
                                }
                                else{
                                    cocktail= cocktailReceived?.let { it1 -> Cocktail(id.toString(), cocktailReceived!!.name,cocktailReceived!!.alcoholic,cocktailReceived!!.ingredients,cocktailReceived!!.instructions, j.image) }
                                    if (cocktail != null) {
                                        i.listOfFavourites.set(position, cocktail)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            updateFile("./src/main/resources/userJson", userList)
            return@put call.respondText("Cocktail modificat correctament a favorits a l'usuari ${call.parameters["username"]}.", status = HttpStatusCode.Accepted)
        }
        delete("/{username}/favourites/{id}"){
            if (call.parameters["username"].isNullOrBlank()){
                return@delete call.respondText("El nom de l'usuari no es correcte.", status = HttpStatusCode.BadRequest)
            }
            else if (call.parameters["id"].isNullOrBlank()){
                return@delete call.respondText("L'id del cocktail no es correcte.", status = HttpStatusCode.BadRequest)
            }
            userList= loadUsersFromFile("./src/main/resources/userJson")
            for (i in userList){
                if (i.username == call.parameters["username"]){
                    barra= loadCocktailsFromFile("./src/main/resources/cocktailsJson")
                    for (j in barra){
                        if(j.id == call.parameters["id"]){
                            i.listOfFavourites.remove(j)
                        }
                    }
                }
            }
            updateFile("./src/main/resources/userJson", userList)
            return@delete call.respondText("Cocktail eliminat correctament a favorits a l'usuari ${call.parameters["username"]}.", status = HttpStatusCode.Accepted)
        }
    }


}
fun loadUsersFromFile(FileRoute: String): MutableList<User> {
    val json = Json { ignoreUnknownKeys = true }
    val fileContent = File(FileRoute).readText()
    userList = json.decodeFromString<MutableList<User>>(fileContent)
    return userList
}

fun updateFile(FileRoute: String, ObjectList: MutableList<User>){
    val json = Json { ignoreUnknownKeys = true }
    val jsonString = json.encodeToString(ObjectList)
    File(FileRoute).writeText(jsonString)
}
