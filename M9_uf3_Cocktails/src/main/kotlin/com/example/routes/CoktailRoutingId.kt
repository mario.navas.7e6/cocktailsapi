package com.example.routes

import com.example.model.Cocktail
import com.example.model.barra
import com.example.model.barraFavorits
import com.example.model.userTable
import com.google.gson.Gson
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

fun Route.cocktailsRoute(){
    authenticate("myAuth") {

        route("cocktails") {
            get {
                println(userTable)
                barra = loadCocktailsFromFile("./src/main/resources/cocktailsJson")
                if (barra.isNotEmpty()) call.respond(barra)
                else call.respondText("No s'han trovat Cocktails.", status = HttpStatusCode.NotFound)
            }

            get("{name?}") {
                if (call.parameters["name"].isNullOrBlank()) {
                    return@get call.respondText(
                        "el nom del cocktail no es correcte.",
                        status = HttpStatusCode.BadRequest
                    )
                }
                barra = loadCocktailsFromFile("./src/main/resources/cocktailsJson")
                for (cocktail in barra) {
                    if (cocktail.name.uppercase() == call.parameters["name"]?.uppercase() || call.parameters["name"]?.uppercase()
                            .toString() in cocktail.name.uppercase()
                    ) return@get call.respond(mutableListOf(cocktail))
                }
                call.respondText(
                    "No s'ha trovat el cocktail amb nom ${call.parameters["name"]} en la barra.",
                    status = HttpStatusCode.NotFound
                )
            }
            post {
                barra = loadCocktailsFromFile("./src/main/resources/cocktailsJson")
                var id = barra.size + 1
                var fileName = ""
                val gson = Gson()
                var cocktailReceived: Cocktail? = null
                val data = call.receiveMultipart()
                data.forEachPart { part ->
                    when (part) {
                        is PartData.FormItem -> {
                            if (part.name == "cocktail_data") {
                                cocktailReceived = gson.fromJson(part.value, Cocktail::class.java)
                            }
                        }

                        is PartData.FileItem -> {
                            fileName = part.originalFileName as String
                            var fileBytes = part.streamProvider().readBytes()
                            File("./src/main/resources/uploads/$fileName").writeBytes(fileBytes)
                        }

                        else -> {}
                    }
                }
                val cocktail = cocktailReceived?.let { it1 ->
                    Cocktail(
                        id.toString(),
                        cocktailReceived!!.name,
                        cocktailReceived!!.alcoholic,
                        cocktailReceived!!.ingredients,
                        cocktailReceived!!.instructions,
                        fileName
                    )
                }
                if (cocktail != null) {
                    barra.add(cocktail)
                }
                updateFile("./src/main/resources/cocktailsJson", barra)
                return@post call.respondText(
                    "Cocktail afegit correctament. L'arxiu \"$fileName s'ha guadat a 'uploads/$fileName'\"",
                    status = HttpStatusCode.Created
                )

            }
            get("/uploads/{imageName}") {
                val imageName = call.parameters["imageName"]
                var file = File("./src/main/resources/uploads/$imageName")
                if (file.exists()) {
                    call.respondFile(File("./src/main/resources/uploads/$imageName"))
                } else {
                    return@get call.respondText("Image not found", status = HttpStatusCode.NotFound)
                }
            }
        }
    }



}
fun loadCocktailsFromFile(FileRoute: String): MutableList<Cocktail> {
    val json = Json { ignoreUnknownKeys = true }
    val fileContent = File(FileRoute).readText()
    barra= json.decodeFromString<MutableList<Cocktail>>(fileContent)
    return barra
}

fun updateFile(FileRoute: String, ObjectList: MutableList<Cocktail>){
    val json = Json { ignoreUnknownKeys = true }
    val jsonString = json.encodeToString(ObjectList)
    File(FileRoute).writeText(jsonString)
}