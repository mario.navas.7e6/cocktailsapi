package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Cocktail (
    var id: String,
    var name: String,
    var alcoholic: Boolean,
    var ingredients: List<String>,
    var instructions: String,
    var image: String
        )

var barra = mutableListOf<Cocktail>()

var barraFavorits= mutableListOf<Cocktail>()