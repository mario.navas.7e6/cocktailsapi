package com.example.plugins

import com.example.routes.cocktailsRoute
import com.example.routes.userRoute
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
            cocktailsRoute()
            userRoute()
    }
}
