# COCKTAIL APP & COCKTAIL API

Aplicacion enlazada con la API CocktailsApi

## Índice

1. Descripción del proyecto
2. Programas necesarios
3. Tecnologias utilizadas
4. Cómo utilizarlo
5. Autores

##  Descripción del proyecto :confused: <a name="1"></a>

    El proyecto consiste en una aplicacion de Android Studio enlazada a una API Rest Ktor.
    La idea es conectar la aplicacion con la api para que de esta forma el usuario pueda 
    interactuar con los datos.

## Programas necesarios  :eyes:

- IntelliJ IDEA
- Android Studio
- Git


## Tecnologias utilizadas :rocket:

- Kotlin
- Git

## Cómo utilizarlo :question:

Se trata de una API en local, por lo tanto el usuario ha de cambiar la ip en la aplicacion Android en el archivo ApiInterface en la variable 'BASE_URL'. Una vez hecho esto, hay que 
iniciar el archivo Application de la api. Todo listo, la aplicacion ya está preparada para interactuar con la Api.

Funcionalidades del usuario:
- Inicio de sesion y registro.
- Listado de cocktails y detalle de estos.
- Añadir y eliminar cocktails a favoritos.
- Añadir cocktail a la lista de cocktails principales.


Estamos trabajando en implementar la función de modificar los cocktails de favoritos.
El usuario no puede eliminar cocktails del listado total de cocktail para evitar un mal uso
de esta función.


## Autores :wave:

DAVID MORENO FERNÁNDEZ
MARIO NAVAS ROPERO